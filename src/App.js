import React from "react";// node\modules配下にあるライブラリは名前だけ
import {Toolbar} from "@material-ui/core";
import {BrowserRouter, Route , Link ,Switch} from "react-router-dom";

import ItemList from "./item-list";
import LoginForm from './login-form';//　自作したコンポーネントは相対パスで指定
import Home from './home'

function App() {
  return (
      <BrowserRouter>
          <div>
              <Toolbar>
                  <Link to='/'>トップ</Link>-
                  <Link to='/login'>ログイン</Link>-
                  <Link to='/list'>一覧</Link>-
              </Toolbar>
              <Switch>
                  <Route exact path ='/' component={Home}/>
                  <Route path = '/login' component={LoginForm}/>
                  <Route path = '/list' component={ItemList}/>
              </Switch>
          </div>
      </BrowserRouter>
  );
};
export default App;
